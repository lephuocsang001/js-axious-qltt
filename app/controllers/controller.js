// render ra man hinh sau khi call api get list user
function renderUserList(userArray) {
  var contentHTML = "";
  userArray.forEach(function (user) {
    var contentTr = `<tr>
                              <td>${user.id}</td>
                              <td>${user.taiKhoan}</td>
                              <td>${user.matKhau}</td>
                              <td>${user.hoTen}</td>
                              <td>${user.email}</td>
                              <td>${user.ngonNgu}</td>
                              <td>${user.loaiND}</td>
                              <td>
                                  <button class="btn btn-danger" onclick="deleleUser(${user.id})"}">
                                  Xoá
                                  </button>
                                  <button class="btn btn-warning" onclick="editUser(${user.id})"}">
                                  Sửa
                                  </button>
                              </td>
                            
                          </tr>`;
    contentHTML += contentTr;
  });

  document.getElementById("tblDanhSachNguoiDung").innerHTML = `${contentHTML}`;
}
// Get data from form
function getDataFromForm() {
  // var id = document.getElementById("id").value;
  var taiKhoan = document.getElementById("TaiKhoan").value;
  var hoTen = document.getElementById("HoTen").value;
  var matKhau = document.getElementById("MatKhau").value;
  var email = document.getElementById("Email").value;
  var loaiND = document.getElementById("loaiNguoiDung").value;
  var ngonNgu = document.getElementById("loaiNgonNgu").value;
  var moTa = document.getElementById("MoTa").value;
  var hinhAnh = document.getElementById("HinhAnh").value;
  //   var us = new User("", _taiKhoan);
  //   console.log(us);
  return new User(
    "",
    taiKhoan,
    hoTen,
    matKhau,
    email,
    loaiND,
    ngonNgu,
    moTa,
    hinhAnh
  );
}

//Show data in form
function ShowDataInForm(user) {
  document.getElementById("id").value = user.id;
  document.getElementById("TaiKhoan").value = user.taiKhoan;
  document.getElementById("HoTen").value = user.hoTen;
  document.getElementById("MatKhau").value = user.matKhau;
  document.getElementById("Email").value = user.email;
  document.getElementById("loaiNguoiDung").value = user.loaiND;
  document.getElementById("loaiNgonNgu").value = user.ngonNgu;
  document.getElementById("MoTa").value = user.moTa;
  document.getElementById("HinhAnh").value = user.hinhAnh;
}
//Close modal
function closeModal() {
  document.getElementById("myModal").classList.remove("show");
  document.getElementById("myModal").style.display = "none";

  document.getElementById("TaiKhoan").value = "";
  document.getElementById("HoTen").value = "";
  document.getElementById("MatKhau").value = "";
  document.getElementById("Email").value = "";
  document.getElementById("loaiNguoiDung").value = "";
  document.getElementById("loaiNgonNgu").value = "";
  document.getElementById("MoTa").value = "";
  document.getElementById("HinhAnh").value = "";
}
// Disabled = true button add new
function addNew() {
  document.getElementById("add").disabled = false;
  document.getElementById("update").disabled = true;

  document.getElementById("title_id").style.display = "none";
}
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
